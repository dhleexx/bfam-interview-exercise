package com.example.marketmaker;

import com.example.marketmaker.exception.QuoteException;
import com.example.marketmaker.model.Quote;
import com.example.marketmaker.service.QuoteParser;
import com.example.marketmaker.service.QuoteProcessor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MarketMakerServer {
    private final ServerSocket serverSocket;
    private final QuoteProcessor quoteProcessor;

    public MarketMakerServer(int port, QuoteProcessor quoteProcessor) throws IOException {
        this.serverSocket = new ServerSocket(port);
        this.quoteProcessor = quoteProcessor;
    }

    public void start() throws IOException {
        ExecutorService threadPool = Executors.newCachedThreadPool();
        while (true) {
            Socket clientSocket = serverSocket.accept();
            threadPool.submit(new RequestHandler(clientSocket));
        }
    }

    public void stop() throws IOException {
        serverSocket.close();
    }

    public class RequestHandler implements Runnable{
        private final Socket clientSocket;
        private PrintWriter out;
        private BufferedReader in;

        public RequestHandler(Socket clientSocket) { this.clientSocket = clientSocket; }

        public void run() {
            try {
                out = new PrintWriter(clientSocket.getOutputStream(), true);
                in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

                String inputLine;
                while ((inputLine = in.readLine()) != null) {
                    try {
                        Quote quote = QuoteParser.getRequest(inputLine);
                        out.println(quoteProcessor.getPrice(quote));
                    } catch (QuoteException e) {
                        System.err.println("Invalid Quote" + e);
                    } catch (Exception e) {
                        System.err.println("Exception occurred during quote" + e);
                    }
                }

                in.close();
                out.close();
                clientSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}