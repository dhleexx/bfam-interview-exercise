package com.example.marketmaker.exception;

public class QuoteException extends Exception {
    private static final long serialVersionUID = 1L;

    public QuoteException(String message) {
        super(message);
    }
}