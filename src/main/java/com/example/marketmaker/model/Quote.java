package com.example.marketmaker.model;

public class Quote {
    int securityId;
    boolean buy;
    int quantity;

    public Quote() {
    }

    public Quote(int securityId, boolean buy, int quantity) {
        this.securityId = securityId;
        this.buy = buy;
        this.quantity = quantity;
    }

    public int getSecurityId() {
        return this.securityId;
    }

    public void setSecurityId(int securityId) {
        this.securityId = securityId;
    }

    public boolean isBuy() {
        return this.buy;
    }

    public void setBuy(boolean buy) {
        this.buy = buy;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}