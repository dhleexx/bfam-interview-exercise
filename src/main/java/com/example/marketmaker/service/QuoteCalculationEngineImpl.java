package com.example.marketmaker.service;

public class QuoteCalculationEngineImpl implements QuoteCalculationEngine {

    @Override
    public double calculateQuotePrice(int securityId, double referencePrice, boolean buy, int quantity) {
        return referencePrice * quantity * (buy ? 1.01 : 0.99);
    }
}
