package com.example.marketmaker.service;

import com.example.marketmaker.exception.QuoteException;
import com.example.marketmaker.model.Quote;
import java.util.StringTokenizer;

public class QuoteParser {
    public QuoteParser() {
    }

    private static boolean isBuy(String position) throws QuoteException {
        if (position.equalsIgnoreCase("buy")) {
            return true;
        } else if (position.equalsIgnoreCase("sell")) {
            return false;
        } else {
            throw new QuoteException("Unrecognized position: " + position);
        }
    }

    private static int validateQuantity(int quantity) throws QuoteException {
        if (quantity < 0) {
            throw new QuoteException("Negative quantity: " + quantity);
        } else {
            return quantity;
        }
    }

    public static Quote getRequest(String request) throws QuoteException {
        StringTokenizer st = new StringTokenizer(request);
        int securityId = Integer.parseInt(st.nextToken());
        boolean buy = isBuy(st.nextToken());
        int quantity = validateQuantity(Integer.parseInt(st.nextToken()));
        if (st.hasMoreTokens()) {
            throw new QuoteException("Unrecognized properties: " + st.nextToken());
        } else {
            return new Quote(securityId, buy, quantity);
        }
    }
}
