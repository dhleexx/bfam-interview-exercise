package com.example.marketmaker.service;

import com.example.marketmaker.exception.QuoteException;
import com.example.marketmaker.model.Quote;

public class QuoteProcessor {
    private final QuoteCalculationEngine quoteCalculationEngine;
    private final ReferencePriceSource referencePriceSource;

    public QuoteProcessor(QuoteCalculationEngine quoteCalculationEngine, ReferencePriceSource referencePriceSource) {
        this.quoteCalculationEngine = quoteCalculationEngine;
        this.referencePriceSource = referencePriceSource;
    }

    public double getPrice(Quote quote) throws QuoteException {
        double referencePrice = validate(this.referencePriceSource.get(quote.getSecurityId()), quote.getSecurityId());

        return this.quoteCalculationEngine.calculateQuotePrice(quote.getSecurityId(), referencePrice, quote.isBuy(), quote.getQuantity());
    }

    private double validate(double price, int securityId) throws QuoteException {
        if(price == -1.0) {
            throw new QuoteException("Security does not exist: " + securityId);
        }

        return price;
    }
}
