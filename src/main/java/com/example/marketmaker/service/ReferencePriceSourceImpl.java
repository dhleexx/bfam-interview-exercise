package com.example.marketmaker.service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

public class ReferencePriceSourceImpl implements ReferencePriceSource {
    private Map<Integer, Double> referencePriceCache = new ConcurrentHashMap<>();
    private List<ReferencePriceSourceListener> listeners = new CopyOnWriteArrayList<>();

    @Override
    public void subscribe(ReferencePriceSourceListener listener) {
        listeners.add(listener);
    }

    @Override
    public double get(int securityId) {
        return referencePriceCache.getOrDefault(securityId, -1.0);
    }

    public void priceUpdated(int securityId, double price){
        referencePriceCache.put(securityId, price);
        listeners.forEach(listener -> listener.referencePriceChanged(securityId, price));
    }
}
