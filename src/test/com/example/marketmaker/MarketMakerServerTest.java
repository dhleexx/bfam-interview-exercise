package com.example.marketmaker;

import com.example.marketmaker.service.*;
import org.junit.jupiter.api.Test;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;

import java.io.IOException;


class MarketMakerServerTest {
    private static MarketMakerServer server;
    private String BUY_QUOTE = "123 BUY 10";
    private String SELL_QUOTE = "123 SELL 10";

    @BeforeTest
    public static void init() {
        ReferencePriceSourceImpl referencePriceSource = new ReferencePriceSourceImpl();
        referencePriceSource.priceUpdated(123, 100);

        QuoteProcessor quoteProcessor = new QuoteProcessor(new QuoteCalculationEngineImpl(), referencePriceSource);

        Runnable runnable = () -> {
            try {
                server = new MarketMakerServer(6666, quoteProcessor);
                server.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        };

        Thread thread = new Thread(runnable);
        thread.start();
    }

    @Test
    public void testQuoteCalculation() throws IOException {
        MarketMakerClient client = new MarketMakerClient();
        client.startConnection("127.0.0.1", 6666);
        String responseBuy = client.sendMessage(BUY_QUOTE);
        Assert.assertEquals(1010, Double.parseDouble(responseBuy));

        String responseSell = client.sendMessage(SELL_QUOTE);
        Assert.assertEquals(990, Double.parseDouble(responseSell));
    }
}